EXACT_NODE = 0
ALPHA_NODE = 1
BETA_NODE = 2

class Entry:
    def __init__(self, zobrist_key, depth, evaluation, flag, best_move):
        self.zobrist_key = zobrist_key
        self.depth = depth
        self.evaluation = evaluation
        self.flag = flag
        self.best_move = best_move


_table_size = 2**20
TABLE = [None] * _table_size


def hash_index(zobrist_key):
    return zobrist_key % _table_size


def get(zobrist_key):
    return TABLE[hash_index(zobrist_key)]


def put(zobrist_key, depth, evaluation, flag, best_move):
    table_index = hash_index(zobrist_key)
    existing_entry = get(zobrist_key) # type: Entry
    if existing_entry is not None and existing_entry.depth > depth:
        return table_index, existing_entry
    entry = Entry(zobrist_key, depth, evaluation, flag, best_move)
    TABLE[table_index] = entry
    return table_index, entry

"""
int ProbeHash(int depth, int alpha, int beta){
    HASHE * phashe = &hash_table[ZobristKey() % TableSize()];
    if (phashe->key == ZobristKey()) {
        if (phashe->depth >= depth) {
            if (phashe->flags == hashfEXACT)
                return phashe->val;
            if ((phashe->flags == hashfALPHA) &&
                (phashe->val <= alpha))
                return alpha;
            if ((phashe->flags == hashfBETA) &&
                (phashe->val >= beta))
                return beta;
        }
        RememberBestMove();
    }
    return valUNKNOWN;
}
"""
def probe_table(zobrist_key, depth, alpha, beta):
    entry = get(zobrist_key)  # type: Entry
    if not entry:
        return None, None
    if entry.zobrist_key == zobrist_key:
        if entry.flag == EXACT_NODE:
            return entry.evaluation, entry
        if entry.flag == ALPHA_NODE and entry.evaluation <= alpha:
            return alpha, entry
        if entry.flag == BETA_NODE and entry.evaluation >= beta:
            return beta, entry
    return None, None

"""
http://web.archive.org/web/20070822204120/www.seanet.com/~brucemo/topics/hashing.htm
"""