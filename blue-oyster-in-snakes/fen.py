from utils import Black, White, Castling


def get_castling_rights(fen: str, side: White or Black):
    pieces = fen.split()
    castling_ability = pieces[2]
    k, q, kq = ('k', 'q', 'kq') if side == Black else ('K', 'Q', 'KQ')
    if '-' == castling_ability:
        return Castling.none
    elif kq in castling_ability:
        return Castling.both
    elif k in castling_ability:
        return Castling.king_side
    else:
        return Castling.queen_side

def get_previous_fifty_move_count(fen):
    pieces = fen.split()
    return int(pieces[4])