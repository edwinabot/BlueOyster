"""
All related to the board representation
"""
import sys
import chess.polyglot as pg

from evaluation import evaluate
import transposition_table as tt




def alpha_beta_max(board, alpha, beta, depth):
    zobrist_key = pg.zobrist_hash(board)
    probe_value = tt.probe_table(zobrist_key, depth, alpha, beta)
    if probe_value: 
        return probe_value

    if not depth:
        return evaluate(board)
    moves = board.legal_moves
    for move in moves:
        board.push(move)
        score = alpha_beta_min(board, alpha, beta, depth - 1)
        board.pop()
        flag = tt.ALPHA_NODE
        if score > alpha:
            flag = tt.EXACT_NODE
            alpha = score
        if score >= beta:
            flag = tt.BETA_NODE
            tt.put(zobrist_key, depth, score, flag, move)
            return beta
        tt.put(zobrist_key, depth, score, flag, move)
    return alpha


def alpha_beta_min(board, alpha, beta, depth):
    zobrist_key = pg.zobrist_hash(board)
    probe_value = tt.probe_table(zobrist_key, depth, alpha, beta)
    if probe_value: 
        return probe_value

    if not depth:
        return evaluate(board)
    moves = board.legal_moves
    for move in moves:
        board.push(move)
        score = alpha_beta_max(board, alpha, beta, depth - 1)
        board.pop()
        flag = tt.BETA_NODE
        if score < beta:
            flag = tt.EXACT_NODE
            beta = score
        if score <= alpha:
            flag = tt.ALPHA_NODE
            tt.put(zobrist_key, depth, score, flag, move)
            return alpha
        tt.put(zobrist_key, depth, score, flag, move)
    return beta

def search(board, depth):

    best_move = None
    is_out_of_book = False

    with pg.open_reader('./Perfect2017.bin') as opening_book:
        try:
            entry = opening_book.weighted_choice(board)
            best_move = entry.move()
        except IndexError:
            is_out_of_book = True

    if is_out_of_book:
        beta = sys.maxsize
        alpha = -sys.maxsize
        minimax = alpha_beta_max if board.turn else alpha_beta_min
        moves = board.legal_moves
        for move in moves:
            board.push(move)
            score = minimax(board, alpha, beta, depth)
            board.pop()
            if board.turn and score > alpha:
                alpha = score
                best_move = move
            if not board.turn and score < beta:
                beta = score
                best_move = move
    return best_move


def alpha_beta(board, alpha, beta, depth):
    """
    A alpha-beta-negamax implementation
    """
    zobrist_key = pg.zobrist_hash(board)
    probe_value, entry = tt.probe_table(zobrist_key, depth, alpha, beta)
    if probe_value: 
        return probe_value

    if not depth:
        return evaluate(board)
    moves = board.legal_moves
    for move in moves:
        board.push(move)
        score = -alpha_beta(board, -beta, -alpha, depth - 1)
        board.pop()
        flag = tt.ALPHA_NODE
        if score >= beta:
            flag = tt.BETA_NODE
            tt.put(zobrist_key, depth, beta, flag, move)
            return beta
        if score > alpha:
            flag = tt.EXACT_NODE
            alpha = score
        tt.put(zobrist_key, depth, score, flag, move)
    return alpha

def negamax_search(board, depth):
    best_move = None
    is_out_of_book = False
    
    with pg.open_reader('./Perfect2017.bin') as opening_book:
        try:
            entry = opening_book.weighted_choice(board)
            best_move = entry.move()
        except IndexError:
            is_out_of_book = True

    if is_out_of_book:
        beta = sys.maxsize
        alpha = -sys.maxsize
        moves = board.legal_moves

        zobrist_key = pg.zobrist_hash(board)
        probe_value, entry = tt.probe_table(zobrist_key, depth, alpha, beta)
        if entry and entry.best_move: 
            return entry.best_move

        for move in moves:
            board.push(move)
            score = alpha_beta(board, alpha, beta, depth)
            board.pop()
            flag = tt.ALPHA_NODE
            if score >= beta:
                flag = tt.BETA_NODE
                tt.put(zobrist_key, depth, beta, flag, move)
            if score > alpha:
                alpha = score
                flag = tt.EXACT_NODE
                best_move = move
                tt.put(zobrist_key, depth, alpha, flag, move)
            tt.put(zobrist_key, depth, score, flag, move)
    return best_move

"""
https://chessprogramming.wikispaces.com/Syzygy+Bases
https://chessprogramming.wikispaces.com/Gaviota%20Tablebases
http://oics.olympuschess.com/tracker/index.php
http://web.archive.org/web/20070822110544/http://www.seanet.com:80/~brucemo/topics/iterative.htm
http://web.archive.org/web/20070809015843/http://www.seanet.com/~brucemo/topics/hashing.htm
"""