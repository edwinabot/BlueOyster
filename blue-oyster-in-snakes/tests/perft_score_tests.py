import unittest

from chess import Board

perft_scores = [1, 20, 400, 8902, 197281, 4865609, 119060324, 3195901860, 84998978956, 2439530234167, 69352859712417]


def count_nodes(board: Board, depth: int):
    nodes = 0
    if not depth:
        return 1
    moves = board.legal_moves
    for move in moves:
        board.push(move)
        nodes += count_nodes(board, depth - 1)
        board.pop()
    return nodes


class InitialPositionPerftTests(unittest.TestCase):

    def setUp(self):
        self.board = Board()

    def test_perft_0(self):
        nodes = count_nodes(self.board, 0)
        self.assertEqual(perft_scores[0], nodes)

    def test_perft_01(self):
        nodes = count_nodes(self.board, 1)
        self.assertEqual(perft_scores[1], nodes)

    def test_perft_02(self):
        nodes = count_nodes(self.board, 2)
        self.assertEqual(perft_scores[2], nodes)

    def test_perft_03(self):
        nodes = count_nodes(self.board, 3)
        self.assertEqual(perft_scores[3], nodes)

    def test_perft_04(self):
        nodes = count_nodes(self.board, 4)
        self.assertEqual(perft_scores[4], nodes)

    def test_perft_05(self):
        nodes = count_nodes(self.board, 5)
        self.assertEqual(perft_scores[5], nodes)

    def test_perft_06(self):
        nodes = count_nodes(self.board, 6)
        self.assertEqual(perft_scores[6], nodes)

    def test_perft_07(self):
        nodes = count_nodes(self.board, 7)
        self.assertEqual(perft_scores[7], nodes)

    def test_perft_08(self):
        nodes = count_nodes(self.board, 8)
        self.assertEqual(perft_scores[8], nodes)

    def test_perft_09(self):
        nodes = count_nodes(self.board, 9)
        self.assertEqual(perft_scores[9], nodes)

    def test_perft_10(self):
        nodes = count_nodes(self.board, 10)
        self.assertEqual(perft_scores[10], nodes)


class KiwipetePositionPerftTests(unittest.TestCase):

    def setUp(self):
        self.nodes = [48, 2039, 97862, 4085603, 193690690]
        self.board = Board(fen="r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0")

    def test_perft_01(self):
        nodes = count_nodes(self.board, 1)
        self.assertEqual(self.nodes[0], nodes)

    def test_perft_02(self):
        nodes = count_nodes(self.board, 2)
        self.assertEqual(self.nodes[1], nodes)

    def test_perft_03(self):
        nodes = count_nodes(self.board, 3)
        self.assertEqual(self.nodes[2], nodes)

    def test_perft_04(self):
        nodes = count_nodes(self.board, 4)
        self.assertEqual(self.nodes[3], nodes)

    def test_perft_05(self):
        nodes = count_nodes(self.board, 5)
        self.assertEqual(self.nodes[4], nodes)

    def test_perft_06(self):
        nodes = count_nodes(self.board, 6)
        self.assertEqual(self.nodes[5], nodes)