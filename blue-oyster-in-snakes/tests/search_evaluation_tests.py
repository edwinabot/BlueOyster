# pylint: disable=C0111, W0614
import unittest

from chess import *

from search import search


class NegaMaxTests(unittest.TestCase):

    def test_mate_in_one(self):
        board = Board(fen=None, chess960=False)
        board.turn = WHITE
        board.set_piece_at(H8, Piece(KING, BLACK))
        board.set_piece_at(G6, Piece(KING, WHITE))
        board.set_piece_at(A1, Piece(QUEEN, WHITE))
        move = search(board, 5)
        board.push(move)
        self.assertTrue(board.is_checkmate())
