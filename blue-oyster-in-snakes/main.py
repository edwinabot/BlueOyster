"""
A main script to play a match
"""
import chess
from search import search, negamax_search

if __name__ == '__main__':
    print('Blue Oyster - A computer chess study by Edwin Abot' \
          '\n==================================================')
    BOARD = chess.Board()
    print(f"{'White' if BOARD.turn else 'Black'} ===== {BOARD.fullmove_number}")
    while not BOARD.is_game_over():
        MOVE = negamax_search(BOARD, 3)
        BOARD.push(MOVE)
        if BOARD.is_check():
            print('CHECK')
        if BOARD.is_checkmate():
            print('CHECKMATE')
        print(BOARD)
        print(f"{'White' if BOARD.turn else 'Black'} ===== {BOARD.fullmove_number}")
