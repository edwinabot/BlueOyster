import itertools

from collections import Counter

from chess import *


WHITE_PAWN_PST = [[ 0,  0,  0,  0,  0,  0,  0,  0],
                  [50, 50, 50, 50, 50, 50, 50, 50],
                  [10, 10, 20, 30, 30, 20, 10, 10],
                  [ 5,  5, 10, 25, 25, 10,  5,  5],
                  [ 0,  0,  0, 20, 20,  0,  0,  0],
                  [ 5, -5,-10,  0,  0,-10, -5,  5],
                  [ 5, 10, 10,-20,-20, 10, 10,  5],
                  [ 0,  0,  0,  0,  0,  0,  0,  0]]


WHITE_KNIGHT_PST = [[-50,-40,-30,-30,-30,-30,-40,-50],
                    [-40,-20,  0,  0,  0,  0,-20,-40],
                    [-30,  0, 10, 15, 15, 10,  0,-30],
                    [-30,  5, 15, 20, 20, 15,  5,-30],
                    [-30,  0, 15, 20, 20, 15,  0,-30],
                    [-30,  5, 10, 15, 15, 10,  5,-30],
                    [-40,-20,  0,  5,  5,  0,-20,-40],
                    [-50,-40,-30,-30,-30,-30,-40,-50]]


WHITE_BISHOP_PST = [[-20,-10,-10,-10,-10,-10,-10,-20],
                    [-10,  0,  0,  0,  0,  0,  0,-10],
                    [-10,  0,  5, 10, 10,  5,  0,-10],
                    [-10,  5,  5, 10, 10,  5,  5,-10],
                    [-10,  0, 10, 10, 10, 10,  0,-10],
                    [-10, 10, 10, 10, 10, 10, 10,-10],
                    [-10,  5,  0,  0,  0,  0,  5,-10],
                    [-20,-10,-10,-10,-10,-10,-10,-20]]


WHITE_ROOK_PST = [[ 0,  0,  0,  0,  0,  0,  0,  0],
                  [ 5, 10, 10, 10, 10, 10, 10,  5],
                  [-5,  0,  0,  0,  0,  0,  0, -5],
                  [-5,  0,  0,  0,  0,  0,  0, -5],
                  [-5,  0,  0,  0,  0,  0,  0, -5],
                  [-5,  0,  0,  0,  0,  0,  0, -5],
                  [-5,  0,  0,  0,  0,  0,  0, -5],
                  [ 0,  0,  0,  5,  5,  0,  0,  0]]


WHITE_QUEEN_PST = [[-20,-10,-10, -5, -5,-10,-10,-20],
                   [-10,  0,  0,  0,  0,  0,  0,-10],
                   [-10,  0,  5,  5,  5,  5,  0,-10],
                   [ -5,  0,  5,  5,  5,  5,  0, -5],
                   [  0,  0,  5,  5,  5,  5,  0, -5],
                   [-10,  5,  5,  5,  5,  5,  0,-10],
                   [-10,  0,  5,  0,  0,  0,  0,-10],
                   [-20,-10,-10, -5, -5,-10,-10,-20]]

WHITE_KING_PSQT = [[-30,-40,-40,-50,-50,-40,-40,-30],
                   [-30,-40,-40,-50,-50,-40,-40,-30],
                   [-30,-40,-40,-50,-50,-40,-40,-30],
                   [-30,-40,-40,-50,-50,-40,-40,-30],
                   [-20,-30,-30,-40,-40,-30,-30,-20],
                   [-10,-20,-20,-20,-20,-20,-20,-10],
                   [ 20, 20,  0,  0,  0,  0, 20, 20],
                   [ 20, 30, 10,  0,  0, 10, 30, 20]]

WHITE_KING_ENDGAME_PST = [[-50,-40,-30,-20,-20,-30,-40,-50],
                          [-30,-20,-10,  0,  0,-10,-20,-30],
                          [-30,-10, 20, 30, 30, 20,-10,-30],
                          [-30,-10, 30, 40, 40, 30,-10,-30],
                          [-30,-10, 30, 40, 40, 30,-10,-30],
                          [-30,-10, 20, 30, 30, 20,-10,-30],
                          [-30,-30,  0,  0,  0,  0,-30,-30],
                          [-50,-30,-30,-30,-30,-30,-30,-50]]

BLACK_PAWN_PST = list(itertools.chain.from_iterable(WHITE_PAWN_PST))
BLACK_KNIGHT_PST = list(itertools.chain.from_iterable(WHITE_KNIGHT_PST))
BLACK_BISHOP_PST = list(itertools.chain.from_iterable(WHITE_BISHOP_PST))
BLACK_ROOK_PST = list(itertools.chain.from_iterable(WHITE_ROOK_PST))
BLACK_QUEEN_PST = list(itertools.chain.from_iterable(WHITE_QUEEN_PST))
BLACK_KING_PSQT = list(itertools.chain.from_iterable(WHITE_KING_PSQT))
BLACK_KING_ENDGAME_PST = list(itertools.chain.from_iterable(WHITE_KING_ENDGAME_PST))

WHITE_PAWN_PST = list(itertools.chain.from_iterable(reversed(WHITE_PAWN_PST)))
WHITE_KNIGHT_PST = list(itertools.chain.from_iterable(reversed(WHITE_KNIGHT_PST)))
WHITE_BISHOP_PST = list(itertools.chain.from_iterable(reversed(WHITE_BISHOP_PST)))
WHITE_ROOK_PST = list(itertools.chain.from_iterable(reversed(WHITE_ROOK_PST)))
WHITE_QUEEN_PST = list(itertools.chain.from_iterable(reversed(WHITE_QUEEN_PST)))
WHITE_KING_PSQT = list(itertools.chain.from_iterable(reversed(WHITE_KING_PSQT)))
WHITE_KING_ENDGAME_PST = list(itertools.chain.from_iterable(reversed(WHITE_KING_ENDGAME_PST)))


def evaluate_pst(board: Board, piece, white_pst, black_pst):
    """
    Evaluates the position for the given Piece-Square tables
    """
    pieces = board.pieces(piece, board.turn)
    pst = white_pst if board.turn else black_pst
    return sum(pst[piece] for piece in pieces)


def get_material_score(board: Board):
    count = dict(Counter(board.piece_map().values()))
    material_score = 20000 * (count.get(Piece.from_symbol('K'), 0) - count.get(Piece.from_symbol('k'), 0)) \
                     + 900 * (count.get(Piece.from_symbol('Q'), 0) - count.get(Piece.from_symbol('q'), 0)) \
                     + 500 * (count.get(Piece.from_symbol('R'), 0) - count.get(Piece.from_symbol('r'), 0)) \
                     + 320 * (count.get(Piece.from_symbol('N'), 0) - count.get(Piece.from_symbol('n'), 0)) \
                     + 330 * (count.get(Piece.from_symbol('B'), 0) - count.get(Piece.from_symbol('b'), 0)) \
                     + 100 * (count.get(Piece.from_symbol('P'), 0) - count.get(Piece.from_symbol('p'), 0))
    
    return material_score


def evaluate(board):
    material_score = get_material_score(board)
    pawns_pst_score = evaluate_pst(board, PAWN, WHITE_PAWN_PST, BLACK_PAWN_PST)
    knights_pst_score = evaluate_pst(board, KNIGHT, WHITE_KNIGHT_PST, BLACK_KNIGHT_PST)
    bishops_pst_score = evaluate_pst(board, BISHOP, WHITE_BISHOP_PST, BLACK_BISHOP_PST)
    rooks_pst_score = evaluate_pst(board, ROOK, WHITE_ROOK_PST, BLACK_ROOK_PST)
    queen_pst_score = evaluate_pst(board, QUEEN, WHITE_QUEEN_PST, BLACK_QUEEN_PST)
    return material_score + pawns_pst_score + knights_pst_score + bishops_pst_score + rooks_pst_score + queen_pst_score

if __name__ == '__main__':
    board = Board()
    evaluation = evaluate(board)
    print(evaluation)
